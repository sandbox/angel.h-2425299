
-- SUMMARY --

The Reveal JS menu module allows the website users to group nodes into menus
which are then displayed on a path as a Reveal JS presentation. Menu items on
levels lower that second will be flattened and will appear as items on level 2.
The user can choose the path and theme in which the presentation is displayed
when they create the presentation entity. Any menu can be displayed as Reveal
JS presentation but for the moment only


-- REQUIREMENTS --

The module requires the Libraries module (https://www.drupal.org/project/libraries)
to load the Reveal JS library (http://lab.hakim.se/reveal-js) which is also required.


-- INSTALLATION --

* Download the Reveal JS library (http://lab.hakim.se/reveal-js) an put it in
  sites/all/libraries.

* Download and enable the Libraries module (https://www.drupal.org/project/libraries).

* Install as usual.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer Reveal.js menus

    Grant this permission to users you want to manage Reveal JS presentations.
    They will be able to create, edit and delete them.

  - Access Reveal JS pages

    Grant this permission to users you want to be able to see your Reveal JS
    presentations. That means that they will have access to the path on which
    the presentation is displayed.

* The actual Reveal JS menus can be managed on Administration » Structure » Reveal JS menus.


-- CONTACT --

Current maintainer:
* Angel Harizanov (angel.h) - https://www.drupal.org/u/angel.h
