<?php

/**
 * @file
 * Administrative page callbacks for Reveal.js Menu module.
 */

/**
 * Page callback for the overview of the Reveal.js menus.
 */
function reveal_js_menu_main_interface() {
  $entities = entity_load('reveal_menu');
  if (count($entities)) {
    $header = array(t('Title'), t('Path'), array(
      'data' => t('Operations'),
      'colspan' => '2',
      ));
    $rows = array();
    foreach ($entities as $entity) {
      $row = array($entity->title);
      $row[] = array('data' => l($entity->path, $entity->path));
      $row[] = array('data' => l(t('edit'), "admin/structure/reveal_menus/{$entity->rmid}/edit"));
      $row[] = array('data' => l(t('delete'), "admin/structure/reveal_menus/{$entity->rmid}/delete"));
      $rows[] = $row;
    }
    return theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    return t('No Reveal JS menus defined yet. Use the link to create new Reveal JS menu.');
  }
}

/**
 * Page callback for the add/edit of a Reveal.js menu.
 */
function reveal_js_menu_edit_form($form, &$form_state, $reveal_menu = NULL) {
  // Prepare some variables for the default values.
  $rmid = empty($reveal_menu) ? 0 : $reveal_menu->rmid;
  $title = '';
  $menu_name = '';
  $path = '';
  $settings = array();
  if (!empty($rmid) && $reveal_menu = entity_load_single('reveal_menu', $rmid)) {
    $title = $reveal_menu->title;
    $menu_name = $reveal_menu->menu_name;
    $path = $reveal_menu->path;
    $settings = $reveal_menu->settings;
  }

  $form = array();

  $form['basic'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Basic setup'),
  );

  $form['basic']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Reveal JS menu title'),
    '#default_value' => $title,
    '#required' => TRUE,
  );

  $form['basic']['menu_name'] = array(
    '#type' => 'select',
    '#title' => t('Menu'),
    '#options' => menu_get_menus(),
    '#default_value' => $menu_name,
    '#description' => t('Select the menu which you want to display as Reveal JS presentation.'),
    '#required' => TRUE,
  );

  $form['basic']['path'] = array(
    '#type' => 'textfield',
    '#default_value' => $path,
    '#title' => t('Display path'),
    '#description' => t('The path on which the Reveal JS will be rendered.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['style'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Style'),
  );

  $themes = _reveal_js_menu_get_reveal_js_themes();
  $form['style']['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => array_combine($themes, $themes),
    '#default_value' => isset($settings['style']['theme']) ? $settings['style']['theme'] : 'black',
    '#description' => t('Choose how you slide will look like.'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('reveal_js_menu_edit_submit'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('reveal_js_menu_edit_cancel'),
  );

  $form['rmid'] = array(
    '#type' => 'hidden',
    '#value' => $rmid,
  );

  return $form;
}

/**
 * Submit callback for the cancel button of the edit Reveal.js menu form.
 */
function reveal_js_menu_edit_cancel($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/reveal_menus';
}

/**
 * Validate callback for the edit operation of the Reveal.js menu form.
 */
function reveal_js_menu_edit_form_validate($form, &$form_state) {
  // The path is required field so just check it's value.
  $path = $form_state['values']['path'];
  if (!valid_url($path) || url_is_external($path)) {
    form_set_error('path', t('The path is not valid.'));
  }
}

/**
 * Submit callback for the edit operation of the Reveal.js menu form.
 */
function reveal_js_menu_edit_submit($form, &$form_state) {
  // Save the Reveal JS menu entity.
  $rmid = isset($form_state['values']['rmid']) ? $form_state['values']['rmid'] : 0;

  $title = $form_state['values']['title'];
  $menu_name = $form_state['values']['menu_name'];
  $path = $form_state['values']['path'];
  $settings = array(
    'style' => array(
      'theme' => $form_state['values']['theme'],
    ),
  );

  $rebuild_menu = TRUE;
  if (!empty($rmid)) {
    $reveal_menu = entity_load_single('reveal_menu', $rmid);

    // Determine if the menu needs to be rebuilt - only if the path was changed.
    $rebuild_menu = $path != $reveal_menu->path;

    $reveal_menu->title = $title;
    $reveal_menu->menu_name = $menu_name;
    $reveal_menu->path = $path;
    $reveal_menu->settings = $settings;
  }
  else {
    $reveal_menu = entity_create('reveal_menu', array(
      'title' => $title,
      'menu_name' => $menu_name,
      'path' => $path,
      'settings' => $settings,
    ));
  }

  entity_save('reveal_menu', $reveal_menu);

  // Rebuild the menu to register the path if needed.
  if ($rebuild_menu) {
    menu_rebuild();
  }

  drupal_set_message(t('Reveal JS menu saved.'));
  $form_state['redirect'] = 'admin/structure/reveal_menus';
}

/**
 * Page callback for the delete operation of menu display.
 */
function reveal_js_menu_delete_form($form, &$form_state, $reveal_menu) {
  $form_state['reveal_menu'] = $reveal_menu;
  return confirm_form($form,
    t('Are you sure you want to delete the Reveal JS menu %name?', array('%name' => $reveal_menu->title)),
    'admin/structure/reveal_menus',
    t('This action cannot be undone.'),
    t('Delete'));
}

/**
 * Delete callback.
 */
function reveal_js_menu_delete_form_submit($form, &$form_state) {
  entity_delete('reveal_menu', $form_state['reveal_menu']->rmid);
  drupal_set_message(t('Reveal JS menu deleted.'));
  $form_state['redirect'] = 'admin/structure/reveal_menus';
}
