<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <title><?php print $title; ?></title>

    <meta name="description" content="<?php print $description; ?>">
    <meta name="author" content="<?php print $username; ?>">

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

    <link rel="stylesheet" href="<?php print $reveal_js_path; ?>css/reveal.css">
    <link rel="stylesheet" href="<?php print $reveal_js_path; ?>css/theme/<?php print $theme_name; ?>.css" id="theme">

    <!-- Code syntax highlighting -->
    <link rel="stylesheet" href="<?php print $reveal_js_path; ?>lib/css/zenburn.css">

    <!-- Printing and PDF exports -->
    <script>
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = "<?php print $reveal_js_path; ?>" + (window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css');
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>

    <!--[if lt IE 9]>
    <script src="<?php print $reveal_js_path; ?>lib/js/html5shiv.js"></script>
    <![endif]-->
</head>

<body>

<div class="reveal">
    <!-- Any section element inside of this container is displayed as a slide -->
    <div class="slides"><?php print $rendered_slides; ?></div>

</div>

<script src="<?php print $reveal_js_path; ?>lib/js/head.min.js"></script>
<script src="<?php print $reveal_js_path; ?>js/reveal.js"></script>

<script>

    // Full list of configuration options available at:
    // https://github.com/hakimel/reveal.js#configuration
    Reveal.initialize({
        controls: true,
        progress: true,
        history: true,
        center: true,

        transition: 'slide', // none/fade/slide/convex/concave/zoom

        // Optional reveal.js plugins
        dependencies: [
            { src: '<?php print $reveal_js_path; ?>lib/js/classList.js', condition: function() { return !document.body.classList; } },
            { src: '<?php print $reveal_js_path; ?>plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
            { src: '<?php print $reveal_js_path; ?>plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
            { src: '<?php print $reveal_js_path; ?>plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
            { src: '<?php print $reveal_js_path; ?>plugin/zoom-js/zoom.js', async: true },
            { src: '<?php print $reveal_js_path; ?>plugin/notes/notes.js', async: true }
        ]
    });

</script>

</body>
</html>
